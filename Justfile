InfluxExportAll := "influx export all"

export-full:
  {{InfluxExportAll}} --filter labelName=raspberry-pi -f raspberry-pi/raspberry-pi.template.yml
  {{InfluxExportAll}} --filter labelName=telegraf -f telegraf/telegraf.template.yml
  {{InfluxExportAll}} --filter labelName=meteoapp -f meteoapp/meteoapp.template.yml

export-all TAG:
  mkdir -p {{TAG}}
  {{InfluxExportAll}} --filter labelName={{TAG}} -f {{TAG}}/{{TAG}}.template.yml

export-meteoapp:
  {{InfluxExportAll}}  --filter labelName=meteoapp -f meteoapp/meteoapp.template.yml

export-telegraf:
  {{InfluxExportAll}} --filter labelName=telegraf -f telegraf/telegraf.template.yml

export-system:
  {{InfluxExportAll}} --filter labelName=raspberry-pi -f raspberry-pi/raspberry-pi.template.yml

